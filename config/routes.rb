Rails.application.routes.draw do
  root 'web_pages#index'
  resources :web_pages do
    member do
      get :words
      get :compare
      get :one_result
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
