class WebPagesController < ApplicationController
  before_action :set_web_page, only: %i[ show edit update destroy words compare one_result]

  # GET /web_pages or /web_pages.json
  def index
    @web_pages = WebPage.all
  end

  # GET /web_pages/1 or /web_pages/1.json
  def show
  end

  # GET /web_pages/new
  def new
    @web_page = WebPage.new
  end

  # GET /web_pages/1/edit
  def edit
  end

  # POST /web_pages or /web_pages.json
  def create
    @web_page = WebPage.new(web_page_params)

    respond_to do |format|
      if @web_page.save
        format.html { redirect_to @web_page, notice: "Web page was successfully created." }
        format.json { render :show, status: :created, location: @web_page }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @web_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /web_pages/1 or /web_pages/1.json
  def update
    respond_to do |format|
      if @web_page.update(web_page_params)
        if request.referrer.split("/").last == "compare"
          format.html { redirect_to one_result_web_page_path(@web_page), notice: "Web page was successfully updated." }
          format.json { render :show, status: :ok, location: @web_page }
        else
          format.html { redirect_to @web_page, notice: "Web page was successfully updated." }
          format.json { render :show, status: :ok, location: @web_page }
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @web_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /web_pages/1 or /web_pages/1.json
  def destroy
    @web_page.destroy
    respond_to do |format|
      format.html { redirect_to web_pages_url, notice: "Web page was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def compare
  end

  def words
    require 'open-uri'
    @doc = Nokogiri::HTML.parse(open("#{@web_page.comparing_url}"){|io| io.read.encode("UTF-8")})
    removed_tags = remove_tags(@doc.text)
    parsed_string = parse_string(removed_tags)
    @words = count_words(parsed_string)
    if has_keywords_tags?(@doc)
      @keywords = get_data_from_meta_tags(@doc, @words)
    else
      @keywords = @words[0..9]
    end
    @count_of_words = 0
    @words.each {|word| @count_of_words += word[1] }
  end

  def one_result
    require 'open-uri'
    @doc1 = Nokogiri::HTML.parse(open("#{@web_page.comparing_url}"){|io| io.read.encode("UTF-8")})
    @doc2 = Nokogiri::HTML.parse(open("#{@web_page.compared_url}"){|io| io.read.encode("UTF-8")})
    @has_metas1 = has_keywords_tags?(@doc1)
    @has_metas2 = has_keywords_tags?(@doc2)
    if has_keywords_tags?(@doc1) && has_keywords_tags?(@doc2)
      removed_tags1 = remove_tags(@doc1.text)
      parsed_string1 = parse_string(removed_tags1)
      @words_counted1 = count_words(parsed_string1)
      @meta1 = get_data_from_meta_tags(@doc1,@words_counted1)   
  
      
      removed_tags2 = remove_tags(@doc2.text)
      parsed_string2 = parse_string(removed_tags2)
      @words_counted2 = count_words(parsed_string2)
      @meta2 = get_data_from_meta_tags(@doc2,@words_counted2)



    elsif has_keywords_tags?(@doc1) || has_keywords_tags?(@doc2)
      if has_keywords_tags?(@doc1)
        removed_tags1 = remove_tags(@doc1.text)
        parsed_string1 = parse_string(removed_tags1)
        @words_counted1 = count_words(parsed_string1)
        @meta1 = get_data_from_meta_tags(@doc1,@words_counted1)

        removed_tags2 = remove_tags(@doc2.text)
        parsed_string2 = parse_string(removed_tags2)
        @words_counted2 = count_words(parsed_string2)
        @meta2 = @words_counted2[0..9]

      else

        removed_tags1 = remove_tags(@doc1.text)
        parsed_string1 = parse_string(removed_tags1)
        @words_counted1 = count_words(parsed_string1)
        @meta1 = @words_counted1[0..9]
        
        removed_tags2 = remove_tags(@doc2.text)
        parsed_string2 = parse_string(removed_tags2)
        @words_counted2 = count_words(parsed_string2)
        @meta2 = get_data_from_meta_tags(@doc2,@words_counted2)

      end

    else

      removed_tags1 = remove_tags(@doc1.text)
      parsed_string1 = parse_string(removed_tags1)
      @words_counted1 = count_words(parsed_string1)
      @meta1 = @words_counted1[0..9]  
  
      
      removed_tags2 = remove_tags(@doc2.text)
      parsed_string2 = parse_string(removed_tags2)
      @words_counted2 = count_words(parsed_string2)
      @meta2 = @words_counted2[0..9]

    end
    @score = calc_score(parsed_string2.count, @meta1[0..9].map{|a| a[0] }, @words_counted2)
  end

  private

    #w2 is the number of words in set 2, meta is the array of keyword and frequency key, value pairs 
    def calc_score(w2, meta1, words_counted_2)
      score = 0
      
      if meta1.length == 10
        meta1[0..2].each{|word1| score += (words_counted_2.map{|kv2| kv2[0] == word1 ? kv2[1] : 0 }.sum * 10) }
        meta1[3..6].each{|word1| score += (words_counted_2.map{|kv2| kv2[0] == word1 ? kv2[1] : 0 }.sum * 5)}
        meta1[7..9].each{|word1| score += (words_counted_2.map{|kv2| kv2[0] == word1 ? kv2[1] : 0 }.sum * 1) }
      else
        meta1.each{|word1| score += (words_counted_2.map{|kv2| kv2[0] == word1 ? kv2[1] : 0 }.sum * 10) }
      end

      score = score.to_f/w2
      score *= 100
    end

    def forbidden_words_to_text
      require 'open-uri'
        doc = Nokogiri::HTML.parse(open("https://www.turkceogretimi.com/tavsiyeler/en-%C3%A7ok-kullan%C4%B1lan-1000-t%C3%BCrk%C3%A7e-kelime"))
        words = []
        doc.xpath("//td").each_with_index do |a,index|
          if index % 3 != 1 && index > 9
          a.text.remove("\n", "\r", /[[:punct:]]/).downcase.split(" ").each {|word| words << word}
          end
         end
        File.open("forbidden_words.txt", "w+") do |f|
          words.each do |word|
            f.write("#{word}, ")
          end
        end
    end

    def forbidden_words
      File.read("forbidden_words.txt").split(", ")
    end


    def has_keywords_tags?(doc)
      !(doc.xpath("//meta[@name='keywords']").blank?)
    end

    def get_data_from_meta_tags(doc, words)
      word_set = doc.xpath("//meta[@name='keywords']").first.values[1].split(",")
      keywords = []
      unless word_set.blank?
        word_set.map{|word| word.split(" ").map{|w| [w.downcase, words.flatten.include?(w.downcase) ? words.select{|c,f| c.downcase == w.downcase }[0][1] : 0]}.each {|kv| keywords << kv}}
      end
      keywords.uniq.sort{|a,b| a[1] <=> b[1]}.reverse.reject{|kv| kv[0].length <4 || kv[0].length >14 || kv[1] < 2 }
    end

    def remove_tags(str)
      removed_tags = ActionView::Base.full_sanitizer.sanitize(str)
      removed_tags
    end

    def parse_string(str)
      parsed_string = []
      forbidden = forbidden_words
      str.remove( /[[:punct:]]/, /\d/, /\d$/ ).split(" ").reject{|e| e.blank? || e.length < 4 || e.length > 15 || e.match(/tag$/) || e.match(/^google/) || e.match(/content$/) || e.match(/header$/) || e.match(/^header/) }.each do  |a|
        unless forbidden.include?(a.downcase)
          parsed_string << a.downcase
        end
      end
      parsed_string
    end

    def count_words(parsed_string)
      words = Hash.new(0)
      parsed_string.each { |iterator| words[iterator] += 1 }
      array_words = words.to_a.sort{|a,b| a[1] <=> b[1]}.reverse
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_web_page
      @web_page = WebPage.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def web_page_params
      params.require(:web_page).permit(:comparing_url, :compared_url)
    end
end
