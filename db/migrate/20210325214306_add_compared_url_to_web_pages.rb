class AddComparedUrlToWebPages < ActiveRecord::Migration[6.0]
  def change
    add_column :web_pages, :compared_url, :text
  end
end
